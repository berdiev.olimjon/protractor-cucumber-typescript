import { by, element } from "protractor";

export async function click(text:string){
    var locator:any;
    if(text.includes('/')){
        locator = by.xpath(text);
    } else {
        locator = by.css(text);
    }

    return new Promise((resolve, reject)=>{
            element(locator).click().then(()=>{
                resolve();
            }).catch((error)=>{
                reject(error);
            });
    });
}