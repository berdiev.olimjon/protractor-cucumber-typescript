/**
 * Created by trphq47 on 11/2/20.
 */
import {defineSupportCode} from "cucumber";
import {browser, element, by, Key} from "protractor";
import { click } from "../utill/BrowserUtill";


defineSupportCode(function ({When, setDefaultTimeout}) {
    setDefaultTimeout(90 * 10000);

    When(/^Start protractor test$/, async() => {
        await browser.get("https://google.com");
        let el = await element(by.name('q'));
        await el.sendKeys('Hello google'+Key.ENTER);
       // await click('(//input[@name="btnK"])[2]');
        await browser.sleep(2000);
    });

    When(/^Start protractor test 2$/, async() => {
        await browser.get("https://amazon.com");
        let el = await element(by.id('twotabsearchtextbox'));
        await el.sendKeys('Hello google'+Key.ENTER);
       // await click('(//input[@name="btnK"])[2]');
        await browser.sleep(2000);
    });

});
