"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.click = void 0;
const protractor_1 = require("protractor");
function click(text) {
    return __awaiter(this, void 0, void 0, function* () {
        var locator;
        if (text.includes('/')) {
            locator = protractor_1.by.xpath(text);
        }
        else {
            locator = protractor_1.by.css(text);
        }
        return new Promise((resolve, reject) => {
            protractor_1.element(locator).click().then(() => {
                resolve();
            }).catch((error) => {
                reject(error);
            });
        });
    });
}
exports.click = click;
