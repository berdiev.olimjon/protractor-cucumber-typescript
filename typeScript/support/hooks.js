"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const yaml = require('js-yaml');
const fs = require('fs');
const { After, Status, Before, defineSupportCode } = require("cucumber");
const devices = protractor_1.browser.params.devices;
Before({ timeout: 30 * 5000 }, () => __awaiter(void 0, void 0, void 0, function* () {
    let currentUrl = yield protractor_1.browser.getCurrentUrl();
    if (!currentUrl.includes('data')) {
        yield protractor_1.browser.restart();
        yield protractor_1.browser.waitForAngularEnabled(false);
        protractor_1.browser.ignoreSynchronization = true;
        yield protractor_1.browser.manage().window().maximize();
        yield protractor_1.browser.manage().timeouts().implicitlyWait(50000);
        yield protractor_1.browser.sleep(5000);
    }
}));
After({ timeout: 10 * 5000 }, () => __awaiter(void 0, void 0, void 0, function* () {
}));
'use strict';
if (devices == 'no') {
    defineSupportCode(({ After }) => {
        After(function (testCase) {
            return __awaiter(this, void 0, void 0, function* () {
                if (testCase.result.status === Status.FAILED) {
                    // Log the spec to the console for protractor-flake to be able to rerun the failed specs
                    try {
                        yield protractor_1.browser.executeScript("document.body.style.zoom='80%'");
                    }
                    catch (err) {
                        console.log(err + ' error in after steps hooks lone 76');
                    }
                    console.log('Specs:', testCase.sourceLocation.uri);
                    const screenShot = yield protractor_1.browser.takeScreenshot();
                    this.attach(screenShot, "image/png");
                }
                return Promise.resolve();
            });
        });
    });
}
